import { mount , flushPromises } from '@vue/test-utils'
import NikeShoes from '../../src/components/NikeShoes.vue'
import shoepai from '../../src/api/shoeapi'

jest.mock('../../src/api/shoeapi')

const titleName = "test"
const priceData = {"shoePrice":3}
jest.spyOn(shoepai, 'getShoePrice').mockResolvedValue(priceData)


describe('display well?', () => {
    const wrapper = mount(NikeShoes,{
        props: {
            title: titleName
        }
    })
    it('display testing ' + priceData.shoePrice, () => {
        const shoeStates = wrapper.findAll('[data-tag="shoe-state"]')
        expect(shoeStates.at(0).text()).not.toBeNull()
        expect(shoeStates.at(0).text()).not.toBeNull()

        const shoePrices = wrapper.findAll('[data-tag="shoe-price"]')
        expect(shoePrices.at(0).text()).not.toBeNull()
        expect(shoePrices.at(0).text()).not.toBeNull()

        const shoeModels = wrapper.findAll('[data-tag="shoe-model"]')
        expect(shoeModels.at(0).text()).not.toBeNull()
    })
})