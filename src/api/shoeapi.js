const header = {
    method: "GET",
    headers: { "Content-Type": "application/json" }
};

function getShoePrice(shoeId) {
    return fetch(`api/shoe-price/`+shoeId, header).then(res => {
        return res.json()
    })
}

function getUserConfig() {
    return fetch(`api/user-config/`, header).then(res=> {return res.json()}).then(res=>{console.log(res)});
}


export default {
    getShoePrice,
    getUserConfig
}